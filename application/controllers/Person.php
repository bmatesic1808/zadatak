<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Person extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('person_model','person');
	}

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('person_view');
	}
    
//------------------------------------------------------------------------------------
//AJAX lista 1
//------------------------------------------------------------------------------------

	public function ajax_list1()
	{
		$list = $this->person->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
            $row[] = $person->id;
			$row[] = $person->ime;
			$row[] = $person->prezime;
			$row[] = $person->telefon;
    
///////////////// Buttoni za kategoriju "Opcije"////////////////////
$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Uredi" onclick="edit_person('."'".$person->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
    
<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Obriši" onclick="delete_person('."'".$person->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            
$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsFiltered" => $this->person->count_filtered(),//ispis samo filtriranih podataka
						"data" => $data,
				);
        
		//prebacivanje podataka u JSON format
		echo json_encode($output);
	}
    
//------------------------------------------------------------------------------------
//AJAX lista 2
//------------------------------------------------------------------------------------
    
    public function ajax_list2()
	{
		$list = $this->person->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
            $row[] = $person->id;
			$row[] = $person->ime;
			$row[] = $person->prezime;
			$row[] = $person->adresa;
    
///////////////// Buttoni za kategoriju "Opcije"////////////////////
$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Uredi" onclick="edit_person('."'".$person->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
    
<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Obriši" onclick="delete_person('."'".$person->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            
$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsFiltered" => $this->person->count_filtered(),//ispis samo filtriranih podataka
						"data" => $data,
				);
        
		//prebacivanje podataka u JSON format
		echo json_encode($output);
	}
    
//------------------------------------------------------------------------------------
//AJAX lista 3
//------------------------------------------------------------------------------------
    public function ajax_list3()
	{
		$list = $this->person->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
            $row[] = $person->id;
			$row[] = $person->ime;
			$row[] = $person->prezime;
			$row[] = $person->grad;
    
///////////////// Buttoni za kategoriju "Opcije"////////////////////
$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Uredi" onclick="edit_person('."'".$person->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
    
<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Obriši" onclick="delete_person('."'".$person->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            
$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsFiltered" => $this->person->count_filtered(),//ispis samo filtriranih podataka
						"data" => $data,
				);
        
		//prebacivanje podataka u JSON format
		echo json_encode($output);
	}
    
//------------------------------------------------------------------------------------
//AJAX FUNKCIJE
//------------------------------------------------------------------------------------
    
	public function ajax_edit($id)
	{
		$data = $this->person->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->_validate();
		$data = array(
				'ime' => $this->input->post('ime'),
				'prezime' => $this->input->post('prezime'),
				'telefon' => $this->input->post('telefon'),
				'adresa' => $this->input->post('adresa'),
				'grad' => $this->input->post('grad'),
			);
		$insert = $this->person->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
				'ime' => $this->input->post('ime'),
				'prezime' => $this->input->post('prezime'),
				'telefon' => $this->input->post('telefon'),
				'adresa' => $this->input->post('adresa'),
				'grad' => $this->input->post('grad'),
			);
		$this->person->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->person->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

    // validacija unosa - sva polja moraju biti popunjena, ukoliko nisu izbaci pogrešku i označi prazno polje
	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('ime') == '')
		{
			$data['inputerror'][] = 'ime';
			$data['error_string'][] = 'Molimo unesite Vaše ime.';
			$data['status'] = FALSE;
		}

		if($this->input->post('prezime') == '')
		{
			$data['inputerror'][] = 'prezime';
			$data['error_string'][] = 'Molimo unesite Vaše prezime';
			$data['status'] = FALSE;
		}

		if($this->input->post('telefon') == '')
		{
			$data['inputerror'][] = 'telefon';
			$data['error_string'][] = 'Molimo unesite Vaš telefonski broj.';
			$data['status'] = FALSE;
		}

		if($this->input->post('adresa') == '')
		{
			$data['inputerror'][] = 'adresa';
			$data['error_string'][] = 'Molimo unesite Vašu adresu.';
			$data['status'] = FALSE;
		}

		if($this->input->post('grad') == '')
		{
			$data['inputerror'][] = 'grad';
			$data['error_string'][] = 'Molimo unesite ime grada u kojem živite.';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
