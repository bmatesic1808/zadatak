<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bruno Matešić - Zadatak</title>
        <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
        <script src="<?php echo base_url('assets/jquery/jquery-3.1.1.min.js')?>"></script>
        <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
        <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
        <link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
        <link rel="stylesheet" href="style.css">
        
        <script type="text/javascript">

                var save_method;
                var table_1;
                var table_2;
                var table_3;

                $(document).ready(function() {

//--------------------------------------------------------------------------------------------
// DATATABLES
//--------------------------------------------------------------------------------------------
// Tablica_1
//--------------------------------------------------------------------------------------------
                        table_1 = $('#table_1').DataTable({ 
                        "language": {
                            "sProcessing": "Učitavanje...",
                            "zeroRecords": "Trenutno nema unosa u bazi.",
                            "search": "_INPUT_",
                            "searchPlaceholder": "Pretraga...",
                            "lengthMenu": 'Rezultata <select>'+
                              '<option value="5">5</option>'+
                              '<option value="10">10</option>'+
                              '<option value="25">25</option>'+
                              '<option value="-1">Sve</option>'+
                              '</select>',
                            "paginate": {
                                "previous": "<<",
                                "next": ">>",
                                },
                        },
                        "iDisplayLength": 5,
                        "responsive": true,
                        "bInfo" : false,
                        "processing": true, //Indikator učitavanja
                        "serverSide": true, 
                        "order": [],

                        // AJAX učitavanje sadržaja tablice
                        "ajax": {
                            "url": "<?php echo site_url('person/ajax_list1')?>",
                            "type": "POST"
                        },

                        "columnDefs": [
                        { 
                            "targets": [-2, -1 ], //odabir stupaca
                            "orderable": false, //onemogući sortiranje po tim stupcima
                        },
                        ],

                    });

//--------------------------------------------------------------------------------------------
// Tablica_2
//--------------------------------------------------------------------------------------------
                        table_2 = $('#table_2').DataTable({
                        "language": {
                            "sProcessing": "Učitavanje...",
                            "zeroRecords": "Trenutno nema unosa u bazi.",
                            "search": "_INPUT_",
                            "searchPlaceholder": "Pretraga...",
                            "lengthMenu": 'Rezultata <select>'+
                                '<option value="5">5</option>'+
                                '<option value="10">10</option>'+
                                '<option value="25">25</option>'+
                                '<option value="-1">Sve</option>'+
                                '</select>',
                            "paginate": {
                                "previous": "<<",
                                "next": ">>",
                                },
                        },
                        "iDisplayLength": 5,
                        "responsive": true,
                        "bInfo" : false,
                        "aLengthMenu": [[5, 10, 25, -1], [5, 10, 25, "Sve"]],
                        "iDisplayLength": 5,
                        "processing": true, //Indikator učitavanja
                        "serverSide": true,
                        "order": [],

                        // AJAX učitavanje sadržaja tablice
                        "ajax": {
                            "url": "<?php echo site_url('person/ajax_list2')?>",
                            "type": "POST"
                        },

                        "columnDefs": [
                        { 
                            "targets": [-2, -1 ], //odabir stupaca
                            "orderable": false, //onemogući sortiranje po tim stupcima
                        },
                        ],

                    });
                    
//--------------------------------------------------------------------------------------------
// Tablica_3
//--------------------------------------------------------------------------------------------
                        table_3 = $('#table_3').DataTable({
                        "language": {
                            "sProcessing": "Učitavanje...",
                            "zeroRecords": "Trenutno nema unosa u bazi.",
                            "search": "_INPUT_",
                            "searchPlaceholder": "Pretraga...",
                            "lengthMenu": 'Rezultata <select>'+
                              '<option value="5">5</option>'+
                              '<option value="10">10</option>'+
                              '<option value="25">25</option>'+
                              '<option value="-1">Sve</option>'+
                              '</select>',
                            "paginate": {
                                "previous": "<<",
                                "next": ">>",
                                },
                        },
                        "iDisplayLength": 5,
                        "responsive": true,
                        "bInfo" : false,
                        "processing": true, //Indikator učitavanja
                        "serverSide": true,
                        "order": [],

                        // AJAX učitavanje sadržaja tablice
                        "ajax": {
                            "url": "<?php echo site_url('person/ajax_list3')?>",
                            "type": "POST"
                        },

                        "columnDefs": [
                        { 
                            "targets": [ -2, -1], //odabir stupaca
                            "orderable": false, //onemogući sortiranje po tim stupcima
                        },
                        ],

                    });

                    $("input").change(function(){
                        $(this).parent().parent().removeClass('has-error');
                        $(this).next().empty();
                    });
                    $("textarea").change(function(){
                        $(this).parent().parent().removeClass('has-error');
                        $(this).next().empty();
                    });
                    $("select").change(function(){
                        $(this).parent().parent().removeClass('has-error');
                        $(this).next().empty();
                    });

                });

//--------------------------------------------------------------------------------------
// AJAX dodavanje podataka u bazu (add)
//--------------------------------------------------------------------------------------
                function add_person()
                {
                    save_method = 'add';
                    $('#form')[0].reset(); // resetiranje forme u modalu
                    $('.form-group').removeClass('has-error'); 
                    $('.help-block').empty();
                    $('#modal_form').modal('show'); // prikaz bootstrap modala
                    $('.modal-title').text('Molimo unesite podatke.'); // naslov modala za unos podataka (add)
                }

//--------------------------------------------------------------------------------------
// AJAX dohvaćanje podataka iz baze (edit forma)
//--------------------------------------------------------------------------------------

                function edit_person(id)
                {
                    save_method = 'update';
                    $('#form')[0].reset(); // resetiranje forme u modalu
                    $('.form-group').removeClass('has-error');
                    $('.help-block').empty();

                    $.ajax({
                        url : "<?php echo site_url('person/ajax_edit/')?>/" + id,
                        type: "GET",
                        dataType: "JSON",
                        success: function(data)
                        {

                            $('[name="id"]').val(data.id);
                            $('[name="ime"]').val(data.ime);
                            $('[name="prezime"]').val(data.prezime);
                            $('[name="telefon"]').val(data.telefon);
                            $('[name="adresa"]').val(data.adresa);
                            $('[name="grad"]').val(data.grad);

                            $('#modal_form').modal('show'); 
                            $('.modal-title').text('Uredi'); // naslov modala za uređivanje (edit)

                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Došlo je do pogreške, molimo pokušajte ponovo!');
                        }
                    });
                }

                function reload_table()
                {
                    table_1.ajax.reload(null,false);
                    table_2.ajax.reload(null,false);
                    table_3.ajax.reload(null,false);//osvježavanje tablice AJAX
                }



//-------------------------------------------------------------------------------------------
// AJAX dodavanje podataka u bazu
//-------------------------------------------------------------------------------------------
                function save()
                {
                    $('#btnSave').text('U redu'); //promjena teksta na buttonu nakon klika
                    $('#btnSave').attr('disabled',true);
                    var url;

                    if(save_method == 'add') {
                        url = "<?php echo site_url('person/ajax_add')?>";
                    } else {
                        url = "<?php echo site_url('person/ajax_update')?>";
                    }

                    $.ajax({
                        url : url,
                        type: "POST",
                        data: $('#form').serialize(),
                        dataType: "JSON",
                        success: function(data)
                        {

                            if(data.status) //ukoliko je uspješno zatvori modal i osvježi tablicu
                            {
                                $('#modal_form').modal('hide');
                                reload_table();
                            }
                            else
                            {
                                for (var i = 0; i < data.inputerror.length; i++) 
                                {
                                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                }
                            }
                            $('#btnSave').text('U redu'); //promjena teksta na buttonu nakon klika
                            $('#btnSave').attr('disabled',false);


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            $('#btnSave').text('U redu');  //promjena teksta na buttonu nakon klika
                            $('#btnSave').attr('disabled',false);

                        }
                    });
            }
            
//-----------------------------------------------------------------------------------------
// brisanje osobe iz baze podataka
//-----------------------------------------------------------------------------------------
                function delete_person(id)
                {
                    if(confirm('Jeste li sigurni da želite obrisati ovu osobu?'))
                    {

                        $.ajax({
                            url : "<?php echo site_url('person/ajax_delete')?>/"+id,
                            type: "POST",
                            dataType: "JSON",
                            success: function(data)
                            {
                                //ukoliko je uspješno, osvježi AJAX tablicu
                                $('#modal_form').modal('hide');
                                reload_table();
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                alert('Došlo je do pogreške! Brisanje neuspješno!');
                            }
                        });

                    }
                }

        </script>
    </head>
<body>
    <div class="container-fluid">
        <div class="row" id="paneli">

<!-- ///////////////////////////////////////   Panel 1 /////////////////////////////////////////// -->
        <div class="col-sm-4">
            <div class="panel panel-striped">
                <div class="panel-body">
                    <table id="table_1" class="table table-responsive table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ime</th>
                                <th>Prezime</th>
                                <th>Telefon</th>
                                <th>Opcije</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<!-- ///////////////////////////////////////   Panel 2 /////////////////////////////////////////// -->
        <div class="col-sm-4">
            <div class="panel panel-striped">
                <div class="panel-body">
                    <table id="table_2" class="table table-responsive table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ime</th>
                                <th>Prezime</th>
                                <th>Adresa</th>
                                <th>Opcije</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<!-- /////////////////////////////////////// Panel 3 /////////////////////////////////////////// -->
        <div class="col-sm-4">
            <div class="panel panel-responsive">
                <div class="panel-body">
                   <table id="table_3" class="table table-responsive table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ime</th>
                                <th>Prezime</th>
                                <th>Grad</th>
                                <th>Opcije</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> <!--class = row, id = paneli-->

<div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4"></div>

        <div class="col-sm-4">
            <button class="btn btn-success btn-block" id="add" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Dodaj novi unos</button>
        </div>
</div>

<div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4"></div>

        <div class="col-sm-4">
        <button class="btn btn-default btn-block" id="reload" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Osvježi</button>
        </div>
</div>

<!-- ////////////////////////////////////////////////Bootstrap modal//////////////////////////////////////////////////// -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Ime</label>
                            <div class="col-md-9">
                                <input name="ime" placeholder="Ime" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Prezime</label>
                            <div class="col-md-9">
                                <input name="prezime" placeholder="Prezime" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Telefonski broj</label>
                            <div class="col-md-9">
                                <input name="telefon" placeholder="Telefonski broj" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Adresa</label>
                            <div class="col-md-9">
                                <input name="adresa" placeholder="Adresa" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Grad</label>
                            <div class="col-md-9">
                                <input name="grad" placeholder="Grad" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">U redu</button>
                <button type="button" id="btnClose" class="btn btn-danger" data-dismiss="modal">Odustani</button>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
    
</body>
</html>